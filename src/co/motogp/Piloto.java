/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.motogp;

/**
 *
 * @author ngrajales
 */
public class Piloto {

    private String numero;
    private String nombre;

    public Piloto(String numero, String nombre) {
        this.numero = numero;
        this.nombre = nombre;
    }

    public String getNumero() {
        return numero;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return "Piloto{" + "numero=" + numero + ", nombre=" + nombre + '}';
    }
    
    

}
