/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.motogp.controler;

import co.motogp.ListaCarrera;
import co.motogp.Nodo;
import co.motogp.Piloto;

/**
 *
 * @author ngrajales
 */
public class ControladorCarrera {

    private ListaCarrera lista_carrera;
    private ListaCarrera lista_pits;
    private ListaCarrera lista_caidos;

    public Nodo encontrar_piloto(Piloto piloto) {
        if (lista_caidos.buscar_nodo(piloto) != null) {
            System.out.println("piloto  Caido= " + piloto);
        } else if (lista_pits.buscar_nodo(piloto) != null) {
            System.out.println("piloto  en pits = " + piloto);
        } else if (lista_carrera.buscar_nodo(piloto) != null) {
            System.out.println("piloto  en carrera = " + piloto);
        } else {
            System.out.println("El piloto no se encuentra registrado");
        }
        return null;
    }

    public void finalizar_carrera() {
        System.out.println("lista_caidos.toString() = " + lista_caidos.toString());
        System.out.println(" lista_carrera.toString() = " + lista_carrera.toString());
        System.out.println("lista_pits.toString() = " + lista_pits.toString());
    }

    public void iniciar_carrera() {
        //aqui creamos los pilotos de la carrera
        System.out.println("Se inicia la carrera ---");
        Piloto obj_piloto1 = new Piloto("46", "VALENTINO ROSSI");
        lista_carrera.adicionar_nodo(new Nodo(obj_piloto1));
        Piloto obj_piloto2 = new Piloto("22", "DANY PEDROSA");
        lista_carrera.adicionar_nodo(new Nodo(obj_piloto2));
        Piloto obj_piloto3 = new Piloto("93", "MARK MARQUEZ");
        lista_carrera.adicionar_nodo(new Nodo(obj_piloto3));
        Piloto obj_piloto4 = new Piloto("13", "CARLOS LOAIZA");
        lista_carrera.adicionar_nodo(new Nodo(obj_piloto4));

        //Se listan los nodos
        //getLista_carrera().lista_nodos();
//        System.out.println("Fin Inicio NODOS --");
//        System.out.println("girar carrera");
//        getLista_carrera().invertir_carrera();
//        getLista_carrera().lista_nodos();
        
        
        
        //Ejercicio 1 --- ingresar el numero de un piloto eliminarlo e insertarlo al final de la lista
        String piloto_buscado = "22";
        System.out.println("se ingreso el numero de piloto " + piloto_buscado);
        lista_carrera.cambiar_posicion_piloto_al_final(piloto_buscado);
        //Ejercicio 2 --- metodo que pone el primero de ultimo y al ultimo de primero
//        lista_carrera.cambiar_ultimo_y_primero();
//        //Ejercicio 3 --- ingresar el numero de un piloto eliminarlo e insertarlo al inicio de la lista
        lista_carrera.cambiar_posicion_piloto_al_inicio(piloto_buscado);
//        //Ejercicio 4 --- metodo que deja de primero a los pilotos pares y de ultimo a los impares
        posicion_par_impar();
        //Ejercicio 5 --- ingresar dos pilotos e intercambiarlos de posicion
//        System.out.println("cambiar de posicion = ");
        lista_carrera.cambiar_posicion_1_2(obj_piloto1, obj_piloto4);
//        lista_carrera.lista_nodos();
        //girar_Carrera();
        //Ejercicio 6 que elimina los pilotos con posicion impar
        lista_carrera.lista_nodos();
        elimiar_posicion_impar();
        lista_carrera.lista_nodos();
//        System.out.println("ADICIONANDO PILOTO");
//        Piloto obj_piloto6 = new Piloto("#06", "Corredor 6");
//        adicionar_piloto(obj_piloto6);
//        
//        getLista_carrera().lista_nodos();
//         System.out.println("Fin Inicio NODOS --");
//    
//        System.out.println("TUMBANDO PILOTO 1");
//        tumbar_piloto(obj_piloto4);
//        
//        System.out.println("TUMBANDO PILOTO 2");
//        tumbar_piloto(obj_piloto1);
//        
//        System.out.println("TUMBANDO PILOTO 3");
//        tumbar_piloto(obj_piloto2);
//        
//        
//        System.out.println("FINALIZOOO LA CARRERA");
//        finalizar_carrera();
//        
//        System.out.println("BUSCANDO PILOTO 111");
//        encontrar_piloto(obj_piloto1);
//        
//        System.out.println("BUSCANDO PILOTO 22");
//        encontrar_piloto(obj_piloto6);
    }

    //metodo para adicionar piloto
    public void adicionar_piloto(Piloto piloto) {
        lista_carrera.adicionar_final_lista(piloto);
    }

    public void tumbar_piloto(Piloto piloto) {
        lista_caidos.adicionar_final_lista(piloto);
        System.out.println("lista_caidos = " + lista_caidos);
        lista_carrera.perder_posicion(piloto);
        System.out.println("lista_carrera = " + lista_carrera);
    }

    public void reingresar_carrera(Piloto piloto) {
        if (lista_caidos.buscar_nodo(piloto) == new Nodo(piloto)) {
            lista_carrera.adicionar_final_lista(piloto);
            lista_caidos.perder_posicion(piloto);
            System.out.println("Se ingresa a la carrera el piloto " + piloto + "Lista Caido");
        } else if (lista_pits.buscar_nodo(piloto) == new Nodo(piloto)) {
            lista_carrera.adicionar_final_lista(piloto);
            lista_pits.perder_posicion(piloto);
            System.out.println("Se ingresa a la carrera el piloto " + piloto + "Lista pits");
        } else if (lista_carrera.buscar_nodo(piloto) == new Nodo(piloto)) {
            System.out.println("Piloto en  la carrera " + piloto + "Lista carrera");
        } else {
            System.out.println("Piloto No encontrado");
        }
    }

    public ControladorCarrera() {
        lista_carrera = new ListaCarrera();
        lista_caidos = new ListaCarrera();
        lista_pits = new ListaCarrera();
        this.iniciar_carrera();
    }

    public void girar_Carrera() {

        Nodo temp = getLista_carrera().getCabeza();
        ListaCarrera lstTemporal = new ListaCarrera();
        while (temp != null) {
            Nodo copia = new Nodo(temp.getObj_piloto());
            lstTemporal.adicionar_nodo_al_inicio(copia);
            temp = temp.getNodo_siguiente();
        }
        setLista_carrera(lstTemporal);
    }

    public void posicion_par_impar() {
        int increme = 1;
        Nodo temp = getLista_carrera().getCabeza();
        ListaCarrera lstTemporal = new ListaCarrera();
        while (temp != null) {
            Nodo tmpcopia = new Nodo(temp.getObj_piloto());
            if (increme % 2 == 0) {
                lstTemporal.adicionar_nodo(tmpcopia);
            } else {
                lstTemporal.adicionar_nodo_al_final(tmpcopia);
            }
            temp = temp.getNodo_siguiente();
            increme++;
        }
        setLista_carrera(lstTemporal);
    }

    public void elimiar_posicion_impar() {
        int increme = 0;
        Nodo temp = getLista_carrera().getCabeza();
        ListaCarrera lstTemporal = new ListaCarrera();
        while (temp != null) {
            Nodo tmpcopia = new Nodo(temp.getObj_piloto());
            if (increme % 2 == 0) {
                lstTemporal.adicionar_nodo(tmpcopia);
            } else {
                System.out.println("posicion impar " + tmpcopia);
            }
            temp = temp.getNodo_siguiente();
            increme++;
        }
        setLista_carrera(lstTemporal);
    }

    public ListaCarrera getLista_carrera() {
        return lista_carrera;
    }

    public void setLista_carrera(ListaCarrera lista_carrera) {
        this.lista_carrera = lista_carrera;
    }

    public ListaCarrera getLista_pits() {
        return lista_pits;
    }

    public void setLista_pits(ListaCarrera lista_pits) {
        this.lista_pits = lista_pits;
    }

    public ListaCarrera getLista_caidos() {
        return lista_caidos;
    }

    public void setLista_caidos(ListaCarrera lista_caidos) {
        this.lista_caidos = lista_caidos;
    }

}
