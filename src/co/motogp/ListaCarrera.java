/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.motogp;

/**
 *
 * @author ngrajales
 */
public class ListaCarrera {

    private Nodo cabeza;

    public void lista_carrera() {
    }

    public void adicionar_nodo(Nodo nodo) {
        //Se revisa que la lista no este vacia
        if (esta_vacia()) {
            setCabeza(nodo);
            cabeza.setNodo_siguiente(null);
        } else {
            nodo.setNodo_siguiente(cabeza);
            cabeza = nodo;
        }

    }

    public void lista_nodos() {
        Nodo tmp_obj = cabeza;
        System.out.println("Listado Nodos --Pilotos --");
        while (tmp_obj != null) {

            System.out.println("# Piloto = " + tmp_obj.getObj_piloto().getNumero() + "Nombre del Piloto = "
                    + tmp_obj.getObj_piloto().getNombre());
            tmp_obj = tmp_obj.getNodo_siguiente();
        }
    }

    public void adelantar_posicion(Nodo nodo, byte posicion) {

    }

    public boolean perder_posicion(Piloto piloto) {
        Nodo tmp_obj = cabeza;
        Nodo tmp_anterior = null;
        int posicion = 1;
        if (buscar_posicion(piloto) != 0) {
            while (tmp_obj != null) {
                if (buscar_posicion(piloto) == posicion) {
                    if (tmp_anterior == null) {
                        cabeza = cabeza.getNodo_siguiente();
                    } else {
                        tmp_anterior.setNodo_siguiente(tmp_obj.getNodo_siguiente());
                    }
                    tmp_obj = null;
                } else {
                    tmp_anterior = tmp_obj;
                    tmp_obj = tmp_obj.getNodo_siguiente();
                    posicion++;
                }
            }
            return true;
        } else {
            System.out.println(" No es una posicion correcta = ");
            return false;
        }
    }

    public void insertar_posicion(Nodo nodo, byte posicion) {
    }

    public void eliminar_nodo(Nodo nodo) {
        if (perder_posicion(nodo.getObj_piloto())) {
            System.out.println("se ha eliminado de la carrera el piloto = " + nodo.getObj_piloto());
        } else {
            System.out.println("no se pudo eliminar el piloto  = " + nodo.getObj_piloto());
        }
    }

    public boolean esta_vacia() {
        return cabeza == null;
    }

    public int size_lista() {
        Nodo tmp_obj = cabeza;
        int size = 0;
        while (tmp_obj != null) {
            size++;
            tmp_obj = tmp_obj.getNodo_siguiente();
        }
        return size;
    }

    public Piloto inicio_lista() {
        if (!esta_vacia()) {
            return cabeza.getObj_piloto();
        }
        return null;
    }

    public Piloto fin_lista() {
        Nodo tpm_obje;
        if (!esta_vacia()) {
            tpm_obje = cabeza;
            while (tpm_obje.getNodo_siguiente() != null) {
                tpm_obje = tpm_obje.getNodo_siguiente();
            }
            return tpm_obje.getObj_piloto();
        }
        return null;
    }

    public Nodo buscar_nodo(Piloto piloto) {
        Nodo tmp_buscar = null;
        Nodo tmp_itera = cabeza;
        while (tmp_buscar == null && tmp_itera != null) {
            if (tmp_itera.getObj_piloto() == piloto) {
                tmp_buscar = tmp_itera;
            }
            tmp_itera = tmp_itera.getNodo_siguiente();
        }
        return tmp_buscar;
//    Nodo tmp_obj = cabeza;
//    int cont = 0;
//    if()
    }

    public int buscar_posicion(Piloto piloto) {
        Nodo tmp_buscar = null;
        Nodo tmp_itera = cabeza;
        int posicion = 0;
        while (tmp_buscar == null && tmp_itera != null) {
            posicion++;
            if (tmp_itera.getObj_piloto() == piloto) {
                tmp_buscar = tmp_itera;
            }
            tmp_itera = tmp_itera.getNodo_siguiente();
        }
        return posicion;
    }

    public void invertir_carrera() {
        Nodo temp = cabeza;
        Nodo temp2;
        while (temp.getNodo_siguiente() != null) {
            temp2 = temp.getNodo_siguiente();
            temp.setNodo_siguiente(temp2.getNodo_siguiente());
            temp2.setNodo_siguiente(cabeza);
            cabeza = temp2;
        }
    }

    public void adicionar_nodo_al_inicio(Nodo nodo) {

        if (getCabeza() == null) {
            setCabeza(nodo);
        } else {
            nodo.setNodo_siguiente(nodo);
            setCabeza(cabeza);
        }
    }

    public Nodo buscar_piloto_por_numero(String n_piloto) {
        Nodo tmp_buscar = null;
        Nodo tmp_itera = cabeza;

        while (tmp_buscar == null && tmp_itera != null) {
            if (tmp_itera.getObj_piloto().getNumero().equals(n_piloto)) {

                tmp_buscar = tmp_itera;
            }
            tmp_itera = tmp_itera.getNodo_siguiente();
        }
        return tmp_buscar;

    }

    public void adicionar_nodo_al_final(Nodo nodo) {
        if (getCabeza() == null) {
            setCabeza(nodo);
        } else {
            Nodo temp = getCabeza();
            while (temp.getNodo_siguiente() != null) {
                temp = temp.getNodo_siguiente();
            }
            temp.setNodo_siguiente(nodo);

        }
    }

    //adiciona por piloto no por nodo
    public void adicionar_final_lista(Piloto piloto) {
        Nodo obj_nuevo = new Nodo(piloto);
        if (esta_vacia()) {
            cabeza = obj_nuevo;
        } else {
            Nodo temp = getCabeza();
            while (temp.getNodo_siguiente() != null) {
                temp = temp.getNodo_siguiente();
            }
            temp.setNodo_siguiente(obj_nuevo);

        }
    }

    public void cambiar_posicion_piloto_al_final(String numero_piloto) {
        Nodo tmp = new Nodo(buscar_piloto_por_numero(numero_piloto).getObj_piloto());
        eliminar_nodo(tmp);
        adicionar_nodo_al_final(tmp);
        lista_nodos();
    }

    public Piloto devolver_primer_piloto() {
        Piloto elemen = null;
        if (!esta_vacia()) {
            elemen = cabeza.getObj_piloto();
        }
        System.out.println("primer elemento " + elemen);
        return elemen;
    }

    public Piloto devolver_ultimo_piloto() {
        Piloto elemen = null;
        Nodo aux;
        if (!esta_vacia()) {
            aux = cabeza;

            //Recorremos
            while (aux.getNodo_siguiente() != null) {
                aux = aux.getNodo_siguiente();
            }
            elemen = aux.getObj_piloto();
        }
        System.out.println("ultimo elemento " + elemen);
        return elemen;
    }

    public void cambiar_ultimo_y_primero() {
        Nodo primero = new Nodo(devolver_primer_piloto());
        Nodo ultimo = new Nodo(devolver_ultimo_piloto());
        eliminar_nodo(primero);
        eliminar_nodo(ultimo);
        adicionar_nodo_al_final(primero);
        adicionar_nodo(ultimo);
        lista_nodos();
    }

    public void cambiar_posicion_1_2(Piloto piloto1, Piloto piloto2) {
        int posicion1 = buscar_posicion(piloto1);
        int posicion2 = buscar_posicion(piloto2);
        modificar_piloto(posicion1, piloto2);
        modificar_piloto(posicion2, piloto1);
    }

    public void modificar_piloto(int posicion, Piloto obj_actual) {
        Nodo tmp_obj = cabeza;
        int sumador = 1;
        if (posicion < 0) {
            System.out.println("Posicion no correcta!!!");
        } else {
            while (tmp_obj != null) {
                if (posicion == sumador) {
                    tmp_obj.setObj_piloto(obj_actual);
                }
                sumador++;
                tmp_obj = tmp_obj.getNodo_siguiente();
            }
        }

    }

    public void cambiar_posicion_piloto_al_inicio(String numero_piloto) {
        Nodo tmp = new Nodo(buscar_piloto_por_numero(numero_piloto).getObj_piloto());
        eliminar_nodo(tmp);
        adicionar_nodo(tmp);
        lista_nodos();
    }

    public Nodo getCabeza() {
        return cabeza;
    }

    public void setCabeza(Nodo cabeza) {
        this.cabeza = cabeza;
    }

    @Override
    public String toString() {
        return "\n Pilotos = {" + cabeza + '}';
    }

}
