/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.motogp;

/**
 *
 * @author ngrajales
 */
public class Nodo {

    private Piloto obj_piloto;
    private Nodo nodo_siguiente;

    public Nodo(Piloto obj_piloto) {
        this.obj_piloto = obj_piloto;
    }

    public Piloto getObj_piloto() {
        return obj_piloto;
    }

    public Nodo getNodo_siguiente() {
        return nodo_siguiente;
    }

    public void setNodo_siguiente(Nodo nodo_siguiente) {
        this.nodo_siguiente = nodo_siguiente;
    }

    public void setObj_piloto(Piloto obj_piloto) {
        this.obj_piloto = obj_piloto;
    }
    

    @Override
    public String toString() {
        return "\nNodo{" + "obj_piloto=" + obj_piloto + ", nodo_siguiente=" + nodo_siguiente + '}';
    }
    
    

}
